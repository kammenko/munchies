package com.firsttask.beginning.repository;


import com.firsttask.beginning.model.entities.Order;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OrdersRepository extends JpaRepository<Order, Long> {

    List<Order> findAllByOrderSentFalse();
    boolean existsByGuid(String guid);
    Order findByGuid(String guid);
}
