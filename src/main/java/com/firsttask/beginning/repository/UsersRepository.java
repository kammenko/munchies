package com.firsttask.beginning.repository;

import com.firsttask.beginning.model.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface UsersRepository extends JpaRepository<User, Long> {
    @Query("Select u from User u where LOWER(u.email) = lower(:email)")
    User findByEmail(@Param("email") String email);
}
