package com.firsttask.beginning.services;

import com.firsttask.beginning.mappers.RestaurantMapper;
import com.firsttask.beginning.model.entities.Restaurant;
import com.firsttask.beginning.model.entities.dto.RestaurantDTO;
import com.firsttask.beginning.repository.RestaurantsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;

import javax.transaction.Transactional;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.UUID;

@Service
@Transactional

@EnableAsync
@EnableScheduling
public class RestaurantService implements IRestaurantService {

    @Autowired
    private RestaurantsRepository repo;
    @Autowired
    RestaurantMapper restM;
    @Autowired
    FileStorageService fileStorageService;

    @Override
    public List<RestaurantDTO> findAll() {

        List<Restaurant> list = repo.findAll();
        List<RestaurantDTO> listDTO = restM.listDTO(list);

        return listDTO;
    }

    @Override
    public boolean doesExiist(Restaurant object) {

        Restaurant rest = repo.existanceCheck(object.getName(), object.getAdress(),
                object.getPhoneNo(), object.getMenuUrl());

        if (rest != null)
            return true;
        else

            return false;
    }

    @Override
    public void save(RestaurantDTO object) throws UnsupportedEncodingException {

        String source = "MUNCHIES" + object.getName();
        byte[] bytes = source.getBytes("UTF-8");
        UUID uuid = UUID.nameUUIDFromBytes(bytes);
        object.setId(uuid.toString());


                repo.save(restM.dtoToEntNew(object));

    }


    @Override
    public void delete(RestaurantDTO object) {


        repo.delete(repo.findByGuid(object.getId()));
        fileStorageService.delete(object.getMenuUrl());

    }

    @Override
    public Restaurant findByRName(String name) {
        return repo.findByRName(name);
    }

    @Override
    public RestaurantDTO findByGuid(String id) {


        return restM.entToDto(repo.findByGuid(id));
    }


    @Override
    @Transactional
    public void update(RestaurantDTO restaurantDTO){

       repo.save(restM.dtoToEnt(restaurantDTO)) ;

    }


    public BindingResult errorsCheck(RestaurantDTO object, BindingResult result) {


        if (repo.findByRName(object.getName())!=null && !object.getId().equals(repo.findByRName(object.getName()).getGuid())){
            result.rejectValue("name", "NotUnique", "Restaurant with name " + object.getName().toUpperCase() + " already exists");
        }

        if(repo.findByAdress(object.getAdress())!=null && !object.getId().equals(repo.findByAdress(object.getAdress()).getGuid())){
            result.rejectValue("adress", "NotUnique","Restaurant with that address already exists" );
        }
        if(repo.findByPhoneNo(object.getPhoneNo())!=null && !object.getId().equals(repo.findByPhoneNo(object.getPhoneNo()).getGuid()))
        {   result.rejectValue("phoneNo", "NotUnique", "Restaurant with that phone number already exists");

        }
        if(repo.findByMenuUrl(object.getMenuUrl())!=null && !repo.findByMenuUrl(object.getMenuUrl()).getGuid().equals(object.getId()))
        {
            result.rejectValue("menuUrl", "NotUnique","Restaurant with that menu already exist");
        }
        if(repo.findByEmail(object.getEmail())!=null && !repo.findByEmail(object.getEmail()).getGuid().equals(object.getId()))
        {
            result.rejectValue("email", "NotUnique","Restaurant with that Email already exist");
        }


        return result;
    }


}



