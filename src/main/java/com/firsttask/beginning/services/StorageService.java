package com.firsttask.beginning.services;

import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

import java.nio.file.Path;
import java.util.stream.Stream;

public interface StorageService {

    void init() throws Exception;
    String store(MultipartFile file, String name) throws Exception;
    Stream<Path> loadAll();
    Path load(String fileName);
    Resource loadAsResource(String fileName) throws Exception;
    void deleteAll();
    void delete(String fileName);
}
