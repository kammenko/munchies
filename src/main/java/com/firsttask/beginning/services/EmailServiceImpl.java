package com.firsttask.beginning.services;

import com.firsttask.beginning.mappers.ItemsMapper;
import com.firsttask.beginning.model.entities.dto.ItemDTO;
import com.firsttask.beginning.model.entities.dto.OrderDTO;
import com.firsttask.beginning.repository.OrdersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

@Service

public class EmailServiceImpl implements EmailService {

  @Autowired

  private JavaMailSender send;

  private OrderService orderService = new OrderService();
 @Autowired
  private OrdersRepository repository;
  private ItemsMapper mapper = new ItemsMapper();
  @Autowired
    TemplateEngine templateEngine;




  public String mailTemplate(List<ItemDTO> list){
      Context context = new Context();

      context.setVariable("Order", list);



      String temp = templateEngine.process("emailTemplate", context );
      double totalPrice = 0;
//      String message="<html><head>"
//              + "<title>"+"Orders from my app"+"</title>"
//              + "</head>"+"<LINK REL='stylesheet' HREF='stylesheet/fac_css.css' TYPE='text/css'>"
//              + "<body>"
//              +"<table    border='0'>"
//              +"<tr><td class ='text12' ><br>I ordered all the following items.Please deliver all the same items on the given below address:</td></tr><tr>"
//              +"<td ></td></tr>"
//              +"<tr><td></td></tr>"
//              +"<tr><td ></td></tr>"
//              +"<tr><td><table border='1'       >"
//              +"<tr  class='centerheading' >"
//              +"<td  ><b>Creator</b></td>"
//              +"<td  ><b>Itme</b></td>"
//              +"<td  ><b>Price</b></td>"
//              +"</tr>";

//      for (int i = 0; i <list.size() ; i++) {
////            message = message + "<tr>" +
////                    "<td >" + list.get(i).getCreator() + "</td>"
////                    + "<td >" + list.get(i).getItem() + "</td>"
////                    + "<td >" + list.get(i).getPrice() + "</td>";
//            totalPrice = totalPrice + list.get(i).getPrice();
//      }
//        message = message+ "</br> TOTAL PRICE: " + totalPrice;
//


      return temp;
  }


    @Override
    public void sendSimpleMessage(String to, String subject, List<ItemDTO> list) throws MailException {


        String   text = "";
        double totalPrice=0;
        SimpleMailMessage message= new SimpleMailMessage();
        for (int i = 0; i <list.size() ; i++) {
            text += "Creator : " + list.get(i).getCreator() + " | Item : " + list.get(i).getItem() + " | Price : " + list.get(i).getPrice() + "|\n";
            totalPrice += list.get(i).getPrice();
        }
        text+="Total price : " + totalPrice;
        message.setTo(to);
        message.setSubject(subject);
        message.setText(text);


        send.send(message);
        OrderDTO order = list.get(0).getOrder();

    }


    @Override
    public void sendTemplateMessage(String to, OrderDTO order) {

        Timer timer  =  new Timer("emailTimer", true);

        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                List<ItemDTO> items = mapper.dtoListNoOrder(repository.findByGuid(order.getId()).getItems());

                MimeMessage message = send.createMimeMessage();
                MimeMessageHelper imHelping = null;
                try {
                    imHelping = new MimeMessageHelper(message, false);
                    imHelping.setTo("kammenko@gmail.com");
                    imHelping.setSubject("Order");
                    imHelping.setText(mailTemplate(items), true);
                    if (!items.isEmpty())
                        send.send(message);
                }
                catch (MessagingException ex){
                    ex.printStackTrace();
                }

            }
        }, order.getEndTime());



//    }
}}
