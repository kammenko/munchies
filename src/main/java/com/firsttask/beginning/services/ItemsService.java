package com.firsttask.beginning.services;

import com.firsttask.beginning.mappers.ItemsMapper;
import com.firsttask.beginning.model.entities.Item;
import com.firsttask.beginning.model.entities.Order;
import com.firsttask.beginning.model.entities.dto.ItemDTO;
import com.firsttask.beginning.repository.ItemsRepository;
import com.firsttask.beginning.repository.OrdersRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
@Service
@Transactional

@EnableAsync
@EnableScheduling
public class ItemsService implements IItemsService {


    @Autowired
    private ItemsRepository repo;
    @Autowired
    private ItemsMapper itemsMapper;


    @Override
    public List<ItemDTO> findAll() {
        return itemsMapper.dtoListNoOrder(repo.findAll());
    }

    @Override
    public ItemDTO saveOrUpdate(ItemDTO object) {

      //  object.setId(null);

       return itemsMapper.toDtoNoOrder(repo.save(itemsMapper.toEnt(object)));

    }

//    @Override
//    public void delete(Items object) {
//        repo.delete(object);
//    }


}

