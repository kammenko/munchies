package com.firsttask.beginning.services;

import com.firsttask.beginning.model.entities.Order;
import com.firsttask.beginning.model.entities.dto.OrderDTO;
import org.quartz.SchedulerException;
import org.springframework.data.crossstore.ChangeSetPersister;

import java.util.List;

public interface IOrdersService /*extends Service<Users>*/ {

     List<OrderDTO> findAll();
     OrderDTO findByGuid(String id) throws NullPointerException;
    OrderDTO saveOrUpdate(OrderDTO object) throws SchedulerException;
     void delete(OrderDTO object) throws ChangeSetPersister.NotFoundException;
    double totalPrice(OrderDTO group);
}
