package com.firsttask.beginning.controllers;

import com.firsttask.beginning.model.entities.dto.OrderDTO;
import com.firsttask.beginning.services.EmailService;
import com.firsttask.beginning.services.EmailServiceImpl;
import com.firsttask.beginning.services.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;


@Controller
@RequestMapping("/mail")
public class MailController {
    @Autowired
    public EmailServiceImpl emailService;
    @Autowired
    private OrderService orderService;


        //MAIL CONTROL//todo promeni poziv i dodaj na order email iz restorana
    @GetMapping(value = "/send/{id}")
    public ModelAndView sendMail(ModelAndView model, @PathVariable("id") String id){

          if(!orderService.findByGuid(id).isOrderSent() && orderService.findByGuid(id).getItems().size()>0)
        emailService.sendSimpleMessage("kammenko@gmail.com", "test", orderService.findByGuid(id).getItems() );
        OrderDTO order =  orderService.findByGuid(id);
          order.setOrderSent(true);
        orderService.saveOrUpdate(order);
        model.setViewName("redirect:/restList");

        return model;
    }

}
