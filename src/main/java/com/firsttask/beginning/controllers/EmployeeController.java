//package com.firsttask.beginning.controllers;
//
//
//import com.firsttask.beginning.model.entities.GroupOrders;
//import com.firsttask.beginning.model.entities.Orders;
//import com.firsttask.beginning.model.entities.Users;
//import com.firsttask.beginning.services.*;
//import com.sun.java.swing.plaf.motif.resources.motif_de;
//import org.hibernate.criterion.Order;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Controller;
//import org.springframework.validation.BindingResult;
//import org.springframework.web.bind.annotation.*;
//import org.springframework.web.servlet.ModelAndView;
//import org.springframework.web.servlet.View;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import javax.validation.Valid;
//import java.time.LocalDateTime;
//import java.time.temporal.ChronoUnit;
//import java.util.Map;
//
//
//@Controller
//@RequestMapping(value = "/")
//public class EmployeeController {
//
//    @Autowired
//    GroupOrderService groupO;
//    @Autowired
//    RestaurantService repoR;
//    @Autowired
//    UserService repoU;
//    @Autowired
//    RoleService repoRo;
//    @Autowired
//    OrderService orderS;
//
//    @RequestMapping(value = "order/{wt}")
//    public ModelAndView order(@PathVariable long wt, Orders order, ModelAndView model) {
//        GroupOrders orders = groupO.findById(wt);
//        order.setGroup_id(orders);
//        //Period time = Period.between(orders.getCreateTime().toLocalDateTime().toLocalDate(), orders.getCreateTime().toLocalDateTime().plusMinutes(orders.getTimeout()).toLocalDate());
//            LocalDateTime s = LocalDateTime.now();
//        long time = ChronoUnit.SECONDS.between(
//               s  , orders.getCreateTime().toLocalDateTime().plusMinutes(orders.getTimeout()));
//
//
//
//        if(order.getGroup_id()!=null && order.getItems()!= null){
//        orderS.saveOrUpdate(order);
//
//        model.addObject("orderList", groupO.findById(order.getGroup_id().getGroup_id()).getOrders());
//        model.addObject("groupO", order.getGroup_id());
//        model.addObject("totalPrice", groupO.totalPrice(order.getGroup_id()));
//        model.setViewName("ordersList");
//        return model;}
//        else {
//            if (LocalDateTime.now().isBefore(groupO.findById(wt).getCreateTime().toLocalDateTime().plusMinutes(groupO.findById(wt).getTimeout()))) {
//                model.addObject("orderTime", "Mozete dodati svoju porudzbinu jos "   + time/60 +":" +  time%60 + " minuta"
//                        );
//
//
//
//
//                model.addObject("grupos", groupO.findById(wt));
//                model.addObject("orderList", groupO.findById(wt).getOrders());
//                model.addObject("totalPrice", groupO.totalPrice(groupO.findById(wt)));
//                model.setViewName("ordersList");
//                return model;
//            }
//            else {
//                model.addObject("message", "This group orders is finished");
//                model.setViewName("error");
//                return model;
//            }
//        }
//    }
//
////    @GetMapping(value = "order")
////    public ModelAndView orderr(Orders order, ModelAndView model, @PathVariable long id) {
////
////
////
////        model.addObject("orderList", groupO.findById(order.getGroup_id().getGroup_id()).getOrders());
////        model.addObject("groupO", groupO.findById(id));
////        model.addObject("totalPrice", groupO.totalPrice(order.getGroup_id()));
////        model.setViewName("ordersList");
////        return model;
////    }
//
////    @PostMapping(value = "finishingOrder")
////    public ModelAndView finish(ModelAndView model){
////        if
////        return null;}
//
//
//    @GetMapping(value = "register")
//    public ModelAndView register(ModelAndView model) {
//
//        Users user = new Users();
//
//        model.addObject("user", user);
//        model.setViewName("registration");
//        return model;
//    }
//
//    @PostMapping(value = "profile")
//    public ModelAndView profile(ModelAndView model, @Valid Users user, BindingResult bindingResult) {
//
//        Users userr = repoU.findByEmail(user.getEmail());
//
//        if (userr != null) {
//            bindingResult.rejectValue("email", "error.user", "Vec ima korisnika sa tim email-om");
//        }
//        if (bindingResult.hasErrors())
//            model.setViewName("registration");
//        else {
//           // user.addRole(repoRo.findAll().get(1));
//            repoU.saveOrUpdate(user);
//            model.addObject("succesMessage", "Uspesno ste se registrovali");
//            model.addObject("user", new Users());
//
//            model.setViewName("profile");
//        }
//
//
//        return model;
//    }
//
//
//    @GetMapping("employee")
//    public ModelAndView employee(ModelAndView model) {
//
//        model.setViewName("employee");
//
//        return model;
//    }
//
//    @PostMapping("groupOrder/{name}")
//    public ModelAndView groupOrder(@PathVariable String name, GroupOrders group, ModelAndView model) {
//
//
//        group.setRestaurant(repoR.findByRName(name));
//        if (group.getCreator().isEmpty())
//            group.setCreator("ANONYMOUS");
//
//        group = groupO.saveOrUpdate(group);
//
//        System.out.println(group.getCreateTime());
//        model.addObject("timestamp", group.getCreateTime().toLocalDateTime().plusMinutes(group.getTimeout()));
//
//
//        model.addObject("groupOrderList", groupO.findAll());
//
//
//        model.setViewName("groupOrderList");
//
//        return model;
//    }
//
//    @RequestMapping("groupOrders")
//    public ModelAndView groupList(ModelAndView model) {
//        model.addObject("groupOrderList", groupO.findAll());
//        model.setViewName("groupOrderList");
//        return model;
//    }
//
//
//    @RequestMapping("grouporder/{id}")
//    public ModelAndView addOrder(@PathVariable long id, ModelAndView model) {
//
//
//        Orders order = new Orders();
//
//        order.setGroup_id(groupO.findById(id));
//
//        model.addObject("order", order);
//        model.setViewName("order");
//
//        return model;
//    }
//
//    @RequestMapping("ordersList/{id}")
//    public ModelAndView listOrders(@PathVariable long id)
//    {
//        ModelAndView model = new ModelAndView();
//        model.addObject("orderList", groupO.findById(id).getOrders());
//        model.addObject("totalPrice", groupO.totalPrice(groupO.findById(id)));
//        model.setViewName("ordersList");
//        return model;
//
//    }
//
//
//    @RequestMapping("totalPrice")
//    public int totalPrice(GroupOrders group){
//       return groupO.totalPrice(group);
//
//    }
//
//
//
//    @RequestMapping("orderDetails/{id}")
//    public ModelAndView gODetail(@PathVariable long id){
//
//        ModelAndView model = new ModelAndView();
//    Orders order = new Orders();
//    order.setGroup_id(groupO.findById(id));
//        model.addObject("order", groupO.findById(id));
//        model.addObject("orders" , order);
//            model.setViewName("groupOrderV2");
//        return model;
//    }
//
//
//    @RequestMapping("addOrder")
//    public ModelAndView aadd(ModelAndView model, GroupOrders orders, Orders order){
//
//        if(order!=null) {
//            order.setGroup_id(groupO.findById(orders.getGroup_id()));
//            orderS.saveOrUpdate(order);
//        }
//
//        model.addObject("order", order.getGroup_id());
//        model.addObject("orders", order)        ;
//    return new ModelAndView("groupOrderV2");
//    }
//
//}
//
//
//
//
//
//
//
//
//
