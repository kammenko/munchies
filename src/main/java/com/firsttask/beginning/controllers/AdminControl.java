package com.firsttask.beginning.controllers;

import com.firsttask.beginning.model.entities.dto.RestaurantDTO;
import com.firsttask.beginning.model.entities.dto.UserDTO;
import com.firsttask.beginning.services.FileStorageService;
import com.firsttask.beginning.services.RestaurantService;
import com.firsttask.beginning.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;

@Controller
@RequestMapping(value = "/admin/")

public class AdminControl {

    @Autowired
    private RestaurantService restS;
    @Autowired
    private UserService userS;
    @Autowired
    private FileStorageService storageService;



    //RESTAURANT/ADMIN CONTROLS//todo proveri validacije
    @GetMapping(value = "addRestaurant")
    public ModelAndView goToAdd(){
        ModelAndView model= new ModelAndView();
        model.setViewName("restaurantAdd");
        model.addObject("newRest", new RestaurantDTO());
        model.addObject("resButt", "Save");
        return model;
    }

    @PostMapping(value = "addRestaurant")
    public ModelAndView addRest(@Valid @ModelAttribute("newRest") RestaurantDTO restaurant,@RequestParam("file") MultipartFile file, BindingResult bindingResult,
                                ModelAndView model) throws Exception {


        bindingResult = restS.errorsCheck(restaurant, bindingResult);
        file.getSize();

        if (bindingResult.hasErrors()){
            model.setViewName("restaurantAdd");
            model.addObject("resButt", "Save");
            model.addObject("newRest", restaurant);
            return  model;

        }
        if(!file.isEmpty()) {
            storageService.store(file, restaurant.getName());
            restaurant.setMenuUrl(storageService.store(file, restaurant.getName()));
        }

        restS.save(restaurant);
        model.setViewName("redirect:/restList");


        return model;

    }

    @GetMapping(value = "editRestaurant/{id}")
    public ModelAndView editRestaurantG(ModelAndView model, RestaurantDTO restaurantDTO, @PathVariable String id){

        model.addObject("resButt", "Update");
        model.addObject("newRest",restS.findByGuid(id));
        model.setViewName("restaurantAdd");
        return model;
    }

    @PostMapping(value = "editRestaurant")
    public ModelAndView editRestaurant(@Valid @ModelAttribute("newRest") RestaurantDTO restaurantDTO, BindingResult bindingResult,
                                        @RequestParam("file") MultipartFile file, ModelAndView model ) throws Exception {

            bindingResult=restS.errorsCheck(restaurantDTO, bindingResult);
        if (bindingResult.hasErrors())
        {
            model.setViewName("restaurantAdd");
            model.addObject("newRest", restaurantDTO);
            model.addObject("resButt", "Update");
            return model;
        }

        if(!file.isEmpty()) {
            storageService.delete(restaurantDTO.getMenuUrl());
            restaurantDTO.setMenuUrl(storageService.store(file, restaurantDTO.getName()));

        }
        restS.update(restaurantDTO);

        model.setViewName("redirect:/restList");
        return model;

    }


    @PostMapping(value = "deleteRestaurant")
    public ModelAndView deleteRest(@Valid RestaurantDTO restaurant){
        restS.delete(restaurant);
        ModelAndView model = new ModelAndView();
        model.setViewName("redirect:/restList");
        return model;
    }

    //ADMIN CONTROLS// //todo user controls
    @GetMapping(value = "addAdmin")
    public ModelAndView adminForm(){
        ModelAndView model = new ModelAndView();
        model.setViewName("registration");
        model.addObject("user", new UserDTO());


        return model;

    }

    @PostMapping(value = "addAdmin")
    public ModelAndView addAdmin(@Valid @ModelAttribute("user") UserDTO user, BindingResult bindingResult, ModelAndView model){
        if(!bindingResult.hasErrors())
        {  userS.saveOrUpdate(user);
        model.setViewName("users");
        model.addObject("userList", userS.findAll());
            return model;}
        else{
            model.addObject("user", user);
            model.setViewName("registration");
        return model;}

    }

    @GetMapping(value = "adminList")
    public ModelAndView userList(){
        ModelAndView model = new ModelAndView();
        model.addObject("userList", userS.findAll());
        model.setViewName("users");
        return model;
    }

}
