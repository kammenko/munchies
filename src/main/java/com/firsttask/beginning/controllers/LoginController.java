package com.firsttask.beginning.controllers;

import com.firsttask.beginning.model.entities.User;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
        @RequestMapping(value = "/" )
public class LoginController {

    @GetMapping(value = "/login")
    public ModelAndView home(){

        ModelAndView model = new ModelAndView();
        model.addObject("user", new User());
        model.setViewName("login");

        return model;
    }




}
