package com.firsttask.beginning.exceptions;

public class OrderExpiredException extends Exception {


    public OrderExpiredException() {
    }

    public OrderExpiredException(String message) {
        super(message);
    }
}
