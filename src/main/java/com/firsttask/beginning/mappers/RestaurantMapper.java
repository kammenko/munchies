package com.firsttask.beginning.mappers;

import com.firsttask.beginning.model.entities.Restaurant;
import com.firsttask.beginning.model.entities.dto.RestaurantDTO;
import com.firsttask.beginning.repository.RestaurantsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class RestaurantMapper {
    @Autowired
    OrdersMapper ordersMapper;
    @Autowired
    RestaurantsRepository restRepo;

    // Map to DTO

    public RestaurantDTO entToDto(Restaurant restaurant) {
        RestaurantDTO restaurantDTO = new RestaurantDTO();


        restaurantDTO.setAdress(restaurant.getAdress());
        restaurantDTO.setPhoneNo(restaurant.getPhoneNo());
        restaurantDTO.setMenuUrl(restaurant.getMenuUrl());
        restaurantDTO.setName(restaurant.getName());
        restaurantDTO.setDeliveryInfo(restaurant.getDeliveryInfo());
        restaurantDTO.setId(restaurant.getGuid());
        restaurantDTO.setEmail(restaurant.getEmail());
        restaurantDTO.setGroupOrders(ordersMapper.dtoListNoRest(restaurant.getGroupOrders()));
        return restaurantDTO;
    }

    public List<RestaurantDTO> listDTO(List<Restaurant> list){
       List<RestaurantDTO> listDTO = new ArrayList<>();
        for (int i = 0; i < list.size() ; i++) {
            listDTO.add(this.entToDto(list.get(i)));


        }

        return listDTO;
    }











// Map from DTO
    public Restaurant dtoToEnt(RestaurantDTO restaurantDTO){

        Restaurant restaurant= new Restaurant();


        restaurant.setAdress(restaurantDTO.getAdress());
        restaurant.setPhoneNo(restaurantDTO.getPhoneNo());
        restaurant.setMenuUrl(restaurantDTO.getMenuUrl());
        restaurant.setName(restaurantDTO.getName());
        restaurant.setDeliveryInfo(restaurantDTO.getDeliveryInfo());
        restaurant.setGuid(restaurantDTO.getId());
        restaurant.setEmail(restaurantDTO.getEmail());
        restaurant.setId(restRepo.findByGuid(restaurantDTO.getId()).getId());
    return restaurant;
    }

    public Restaurant dtoToEntNew(RestaurantDTO restaurantDTO){

        Restaurant restaurant= new Restaurant();


        restaurant.setAdress(restaurantDTO.getAdress());
        restaurant.setPhoneNo(restaurantDTO.getPhoneNo());
        restaurant.setMenuUrl(restaurantDTO.getMenuUrl());
        restaurant.setName(restaurantDTO.getName());
        restaurant.setDeliveryInfo(restaurantDTO.getDeliveryInfo());
        restaurant.setGuid(restaurantDTO.getId());
        restaurant.setEmail(restaurantDTO.getEmail());
        //restaurant.setId(restRepo.findByGuid(restaurantDTO.getId()).getId());
        return restaurant;
    }

}