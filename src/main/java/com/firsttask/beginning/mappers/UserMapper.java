package com.firsttask.beginning.mappers;

import com.firsttask.beginning.model.entities.User;
import com.firsttask.beginning.model.entities.dto.UserDTO;
import org.springframework.stereotype.Component;

@Component
public class UserMapper {

    public UserDTO entToDTO(User user) {

        UserDTO userDTO = new UserDTO();
        userDTO.setId(user.getId());
        userDTO.setEmail(user.getEmail());
        userDTO.setFirstName(user.getFirstName());
        userDTO.setLastName(user.getLastName());
        userDTO.setPassword(user.getPassword());


        return userDTO;


    }


    public User dtoToEnt(UserDTO userDTO){
        User user = new User();
        user.setEmail(userDTO.getEmail());
        user.setFirstName(userDTO.getFirstName());
        user.setId(userDTO.getId());
        user.setLastName(userDTO.getLastName());
        user.setPassword(userDTO.getPassword());
        return user;

    }

}
