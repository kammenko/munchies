package com.firsttask.beginning.model.entities;


import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@NamedQueries({
        @NamedQuery(name = "User.findByEmail", query = "select  u from User u where u.email = :email")}
)
@Table(name = "users")
public class User {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Column(name = "email", unique = true)

    private String email;
    @Column(name = "password")

    private String password;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")

    private String lastName;
    @ManyToMany( fetch = FetchType.EAGER ,cascade = CascadeType.ALL)
    @JoinTable(name = "users_roles" ,
               joinColumns = @JoinColumn(name = "users"),
                inverseJoinColumns = @JoinColumn(name = "roles"))

    private List<Role> role = new ArrayList<>();

    public User() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public List<Role> getRole() {
        return role;
    }

    public void setRole(List<Role> role) {
        this.role = role;
    }

    @Override
    public String toString() {
        return "Users{" +
                "email='" + email + '\'' +
                ", first_name='" + firstName + '\'' +
                ", last_name='" + lastName + '\'' +
                '}';
    }
}
