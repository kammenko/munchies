package com.firsttask.beginning.model.entities;


import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;


@Entity
@Table(name = "restaurants")
public class Restaurant {



    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY )
    private long id;

    @Column(name = "guid")
    private String guid;

    @Column(name = "restaurant_name", unique = true)

    private String name;

    @Column(name="adress", unique = true)


    private String adress;

    @Column(name = "phone_no", unique = true)

    private String phoneNo;

    @Column(name= "menu_url", unique = true)

    private String menuUrl;

    @Column(name = "delivery_info")


    private String deliveryInfo;

    @Column(name = "email")
    private String email;

    @OneToMany(mappedBy = "restaurant", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<Order> groupOrders = new ArrayList<>();

    public Restaurant() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getGuid() {
        return guid;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAdress() {
        return adress;
    }

    public void setAdress(String adress) {
        this.adress = adress;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phone_number) {
        this.phoneNo = phone_number;
    }

    public String getMenuUrl() {
        return menuUrl;
    }

    public void setMenuUrl(String menu_url) {
        this.menuUrl = menu_url;
    }

    public String getDeliveryInfo() {
        return deliveryInfo;
    }

    public void setDeliveryInfo(String delivery_info) {
        this.deliveryInfo = delivery_info;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<Order> getGroupOrders() {
        return groupOrders;
    }

    public void setGroupOrders(List<Order> groupOrders) {
        this.groupOrders = groupOrders;
    }

//    public void cloneRestaurant(Restaurants restaurant){
//        this.setId(restaurant.getId());
//        this.setPhone_no(restaurant.getPhone_no());
//        this.setMenu_url(restaurant.getMenu_url());
//        this.setAdress(restaurant.getAdress());
//        this.setName(restaurant.getName());
//        this.setDelivery_info(restaurant.getDelivery_info());
//        this.setGroupOrders(restaurant.getGroupOrders());
//
//
//    }
}
