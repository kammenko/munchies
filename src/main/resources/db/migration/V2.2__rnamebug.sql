alter table restaurants add column restaurant_name varchar(200) not null ;
alter table restaurants add column guid varchar(80);
alter table users add column guid varchar(80);
alter table order_items add column guid varchar(80);
alter table orders add column guid varchar(80);
