Alter table users add role_id int(11) default '2' ;
alter table users add constraint FKUSERROLE foreign key (role_id) references roles(role_id);
CREATE table users_roles(
user_id int,
role_id int ,
primary key (user_id, role_id),
constraint fk1 foreign key (user_id) references users(user_id),
constraint fk2 foreign key (role_id) references roles(role_id)
);