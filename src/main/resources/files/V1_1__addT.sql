Alter TABLE orders add group_id int(11);

ALTER TABLE orders ADD CONSTRAINT Ordersfk FOREIGN KEY (group_id) REFERENCES grouporders(group_id);

